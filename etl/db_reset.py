from sqlalchemy import create_engine
from sqlalchemy.sql import text

from config import db_info


TABLES = [
    'shifts',
    'breaks',
    'allowances',
    'award_interpretations',
    'kpis',
]
engine = create_engine(
    db_info,
    echo=True,
)

with engine.connect() as con:
    for table in TABLES:
        con.execute(text(f'DELETE FROM {table}'))
