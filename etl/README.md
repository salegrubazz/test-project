# ETL API

Gets, generates and saves data to a PostgreSQL database.

## Getting Started

Install dependencies inside virtual environment

Dependencies can be found in requirements.txt inside /etl

Run the module by starting main.py

db_reset.py is used for reseting the database

## Approach

Python was used over Java as the programming language of choice due to broader usage experience.

SqlAlchemy was used for the communication with the PostgreSQL database.
SqlAlchemy is a Python SQL toolkit and object relational mapper. It was utilized due to it's ORM feature and the fact that raw SQL is not needed for it to function.
It works in combination with psycopg2 which is a PostgreSQL database adapter for Python.

The app is divided into 3 modules:
1) config.py contains url and database connection configurations. It makes it easy to change those configurations.
2) models.py contains all mapping informations for SqlAlchemy
3) main.py which calls the other 2 modules and is responsible for getting, transforming and sending data to the database.
