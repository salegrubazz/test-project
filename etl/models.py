from sqlalchemy import (
    Column,
    NUMERIC,
    ForeignKey,
    String,
    Boolean,
    Integer,
    Date,
    TIMESTAMP
)
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Shift(Base):
    __tablename__ = 'shifts'

    shift_id = Column(UUID, primary_key=True)
    shift_date = Column(Date, nullable=False)
    shift_start = Column(TIMESTAMP, nullable=False)
    shift_finish = Column(TIMESTAMP, nullable=False)
    shift_cost = Column(NUMERIC)


class Break(Base):
    __tablename__ = 'breaks'

    break_id = Column(UUID, primary_key=True)
    shift_id = Column(UUID, ForeignKey('shifts.shift_id'))
    break_start = Column(TIMESTAMP, nullable=False)
    break_finish = Column(TIMESTAMP, nullable=False)
    is_paid = Column(Boolean)


class Allowance(Base):
    __tablename__ = 'allowances'

    allowance_id = Column(UUID, primary_key=True)
    shift_id = Column(UUID, ForeignKey('shifts.shift_id'))
    allowance_value = Column(NUMERIC)
    allowance_cost = Column(NUMERIC)


class Award(Base):
    __tablename__ = 'award_interpretations'

    award_id = Column(UUID, primary_key=True)
    shift_id = Column(UUID, ForeignKey('shifts.shift_id'))
    award_date = Column(Date, nullable=False)
    award_units = Column(NUMERIC)
    award_cost = Column(NUMERIC)


class KPI(Base):
    __tablename__ = 'kpis'

    kpi_id = Column(Integer, primary_key=True)
    kpi_name = Column(String)
    kpi_date = Column(Date, nullable=False)
    kpi_value = Column(NUMERIC)
