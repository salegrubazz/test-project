import json
import config
import models
from urllib.request import urlopen

from datetime import date, datetime
from statistics import mean

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


# this function gets json data from app as a python dictionary
def retrieve_data(url):
    with urlopen(url) as response:
        response = response.read().decode('UTF-8')
        data = json.loads(response)
    return data


# this function initilazes database connection
def start_db(info):
    engine = create_engine(info, echo=True,)
    session_maker = sessionmaker(
        autocommit=False, autoflush=False, bind=engine
        )
    db = session_maker()
    return db


# this function adds items to database
def add_to_db(name, db):
    db.add(name)
    db.commit()
    db.refresh(name)


# this function saves KPI data to the database
def kpi_db(name, value, db):
    kpi = models.KPI(
        kpi_name=name,
        kpi_date=date.today(),
        kpi_value=value,
    )
    add_to_db(kpi, db)


# this funciont finds the 14th day before the last shift day
def border_day_fun(big_data):
    latest_day = 0
    for entry in big_data:
        if entry.get('finish') > latest_day:
            latest_day = entry.get('finish')
    return latest_day - 1209600000


# this function transforms and saves all data to the database
def data_generator(big_data, db):
    mean_break_length_in_minutes = []
    mean_shift_cost = []
    max_allowance = []
    min_shift_len = []
    num_paid_breaks = 0
    conseq_break_counter = 0
    max_brake_counter = 0
    border_day = (border_day_fun(big_data))

    for entry in big_data:
        id = entry.get('id')

        shifts_cost = 0
        for elem in entry.get('allowances'):
            allowances_cost = elem.get('cost')
            shifts_cost = round(shifts_cost + allowances_cost, 1)

        for elem in entry.get('award_interpretations'):
            award_cost = elem.get('cost')
            shifts_cost = round(shifts_cost + award_cost, 1)

        mean_shift_cost.append(shifts_cost)

        min_shift_len.append(entry.get('finish') - entry.get('start'))

        shift = models.Shift(
            shift_id=id,
            shift_date=entry.get('date'),
            shift_start=datetime.fromtimestamp(entry.get('start')/1000.0),
            shift_finish=datetime.fromtimestamp(entry.get('finish')/1000.0),
            shift_cost=shifts_cost,
        )
        add_to_db(shift, db)

        for elem in entry.get('allowances'):
            if entry.get('start') > border_day:
                max_allowance.append(elem.get('cost'))

            allowance = models.Allowance(
                allowance_id=elem.get('id'),
                shift_id=id,
                allowance_value=elem.get('value'),
                allowance_cost=elem.get('cost'),
            )
            add_to_db(allowance, db)

        for elem in entry.get('award_interpretations'):
            award = models.Award(
                award_id=elem.get('id'),
                shift_id=id,
                award_date=elem.get('date'),
                award_units=elem.get('units'),
                award_cost=elem.get('cost'),
            )
            add_to_db(award, db)

        if entry.get('breaks') == []:
            conseq_break_counter = conseq_break_counter + 1
            if conseq_break_counter > max_brake_counter:
                max_brake_counter = conseq_break_counter
        else:
            conseq_break_counter = 0

        for elem in entry.get('breaks'):
            if elem.get('paid'):
                num_paid_breaks = num_paid_breaks + 1
            breaks = models.Break(
                break_id=elem.get('id'),
                shift_id=id,
                break_start=datetime.fromtimestamp(elem.get('start')/1000.0),
                break_finish=datetime.fromtimestamp(elem.get('finish')/1000.0),
                is_paid=elem.get('paid'),
            )
            add_to_db(breaks, db)
            break_time = elem.get('finish') - elem.get('start')
            mean_break_length_in_minutes.append((break_time)/60000)

    kpi_db('mean_break_length_in_minutes', mean(
        mean_break_length_in_minutes), db
        )
    kpi_db('mean_shift_cost', mean(mean_shift_cost), db)
    kpi_db('max_allowance_cost_14d', max(max_allowance), db)
    kpi_db('max_break_free_shift_period_in_days', max_brake_counter, db)
    kpi_db('min_shift_length_in_hours', min(min_shift_len)/3600000, db)
    kpi_db('total_number_of_paid_breaks', num_paid_breaks, db)


def main():
    db = start_db(config.db_info)

    data = retrieve_data(config.app_url)

    data_generator(data.get('results'), db)

    db.close()


if __name__ == '__main__':
    main()
